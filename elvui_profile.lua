{
    ["general"] = {
        ["totems"] = {
            ["growthDirection"] = "HORIZONTAL",
            ["size"] = 50,
            ["spacing"] = 8,
        },
        ["valuecolor"] = {
            ["b"] = 0.99,
            ["g"] = 0.99,
            ["r"] = 0.99,
        },
        ["bottomPanel"] = false,
        ["bonusObjectivePosition"] = "AUTO",
        ["talkingHeadFrameScale"] = 1,
        ["bordercolor"] = {
            ["b"] = 0,
            ["g"] = 0,
            ["r"] = 0,
        },
        ["objectiveFrameHeight"] = 400,
        ["minimap"] = {
            ["size"] = 220,
        },
    },
    ["layoutSetting"] = "dpsCaster",
    ["tooltip"] = {
        ["healthBar"] = {
            ["height"] = 12,
            ["fontOutline"] = "MONOCHROMEOUTLINE",
        },
    },
    ["chat"] = {
        ["panelWidth"] = 472,
        ["tabFontSize"] = 10,
        ["panelHeight"] = 236,
    },
    ["unitframe"] = {
        ["smoothbars"] = true,
        ["colors"] = {
            ["healthclass"] = true,
            ["castClassColor"] = true,
            ["auraBarBuff"] = {
                ["b"] = 0.99,
                ["g"] = 0.99,
                ["r"] = 0.99,
            },
        },
        ["thinBorders"] = true,
        ["units"] = {
            ["pet"] = {
                ["debuffs"] = {
                    ["enable"] = true,
                    ["anchorPoint"] = "TOPRIGHT",
                },
                ["disableTargetGlow"] = false,
                ["width"] = 270,
                ["infoPanel"] = {
                    ["height"] = 14,
                },
                ["castbar"] = {
                    ["iconSize"] = 32,
                },
            },
            ["targettarget"] = {
                ["debuffs"] = {
                    ["enable"] = false,
                    ["anchorPoint"] = "TOPRIGHT",
                },
                ["power"] = {
                    ["enable"] = false,
                },
                ["disableMouseoverGlow"] = true,
                ["width"] = 270,
                ["threatStyle"] = "GLOW",
                ["raidicon"] = {
                    ["attachTo"] = "LEFT",
                    ["xOffset"] = 2,
                    ["enable"] = false,
                    ["yOffset"] = 0,
                },
            },
            ["player"] = {
                ["classbar"] = {
                    ["height"] = 14,
                },
                ["health"] = {
                    ["attachTextTo"] = "InfoPanel",
                },
                ["power"] = {
                    ["height"] = 22,
                    ["attachTextTo"] = "InfoPanel",
                },
                ["disableMouseoverGlow"] = true,
                ["infoPanel"] = {
                    ["enable"] = true,
                },
                ["castbar"] = {
                    ["insideInfoPanel"] = false,
                    ["height"] = 40,
                    ["width"] = 405,
                },
                ["height"] = 82,
            },
            ["raid40"] = {
                ["enable"] = false,
                ["rdebuffs"] = {
                    ["font"] = "PT Sans Narrow",
                },
            },
            ["focus"] = {
                ["castbar"] = {
                    ["width"] = 270,
                },
                ["width"] = 270,
            },
            ["target"] = {
                ["name"] = {
                    ["attachTextTo"] = "InfoPanel",
                },
                ["disableMouseoverGlow"] = true,
                ["health"] = {
                    ["attachTextTo"] = "InfoPanel",
                },
                ["infoPanel"] = {
                    ["enable"] = true,
                },
                ["height"] = 82,
                ["power"] = {
                    ["height"] = 22,
                    ["attachTextTo"] = "InfoPanel",
                },
                ["orientation"] = "LEFT",
                ["castbar"] = {
                    ["insideInfoPanel"] = false,
                    ["height"] = 40,
                    ["width"] = 405,
                },
            },
            ["raid"] = {
                ["rdebuffs"] = {
                    ["font"] = "PT Sans Narrow",
                    ["size"] = 30,
                    ["xOffset"] = 30,
                    ["yOffset"] = 25,
                },
                ["growthDirection"] = "RIGHT_UP",
                ["resurrectIcon"] = {
                    ["attachTo"] = "BOTTOMRIGHT",
                },
                ["roleIcon"] = {
                    ["xOffset"] = 0,
                    ["attachTo"] = "InfoPanel",
                    ["size"] = 12,
                },
                ["width"] = 92,
                ["infoPanel"] = {
                    ["enable"] = true,
                },
                ["name"] = {
                    ["attachTextTo"] = "InfoPanel",
                    ["xOffset"] = 2,
                    ["position"] = "BOTTOMLEFT",
                },
                ["numGroups"] = 8,
                ["visibility"] = "[@raid6,noexists] hide;show",
            },
            ["boss"] = {
                ["debuffs"] = {
                    ["maxDuration"] = 300,
                    ["sizeOverride"] = 27,
                    ["yOffset"] = -16,
                },
                ["width"] = 246,
                ["infoPanel"] = {
                    ["height"] = 17,
                },
                ["castbar"] = {
                    ["width"] = 246,
                },
                ["height"] = 60,
                ["buffs"] = {
                    ["maxDuration"] = 300,
                    ["sizeOverride"] = 27,
                    ["yOffset"] = 16,
                },
            },
            ["party"] = {
                ["rdebuffs"] = {
                    ["font"] = "PT Sans Narrow",
                },
                ["power"] = {
                    ["height"] = 13,
                },
                ["width"] = 231,
                ["height"] = 74,
            },
        },
    },
    ["datatexts"] = {
        ["panels"] = {
            ["LeftChatDataPanel"] = {
                [3] = "Quick Join",
            },
        },
    },
    ["actionbar"] = {
        ["bar3"] = {
            ["buttonsize"] = 50,
            ["buttons"] = 8,
            ["buttonsPerRow"] = 10,
            ["visibility"] = "[petbattle] hide; show",
            ["buttonspacing"] = 1,
        },
        ["bar6"] = {
            ["enabled"] = true,
            ["buttonsPerRow"] = 1,
            ["visibility"] = "[petbattle] hide; show",
        },
        ["bar1"] = {
            ["buttonspacing"] = 1,
            ["buttonsize"] = 50,
            ["buttons"] = 8,
        },
        ["bar5"] = {
            ["buttonsPerRow"] = 3,
            ["visibility"] = "[petbattle] hide; show",
        },
        ["bar2"] = {
            ["enabled"] = true,
            ["buttons"] = 9,
            ["buttonspacing"] = 1,
            ["buttonsize"] = 38,
            ["visibility"] = "[petbattle] hide; show",
        },
        ["bar4"] = {
            ["point"] = "BOTTOMLEFT",
            ["buttons"] = 6,
            ["buttonspacing"] = 1,
            ["buttonsPerRow"] = 3,
            ["backdrop"] = false,
            ["visibility"] = "[petbattle] hide; show",
        },
    },
    ["v11NamePlateReset"] = true,
    ["bags"] = {
        ["scrapIcon"] = true,
        ["bankWidth"] = 474,
        ["bagWidth"] = 474,
        ["bankSize"] = 42,
        ["itemLevelCustomColorEnable"] = true,
        ["bagSize"] = 42,
    },
    ["movers"] = {
        ["TopCenterContainerMover"] = "TOP,ElvUIParent,TOP,0,-30",
        ["PetAB"] = "TOPRIGHT,ElvUIParent,TOPRIGHT,-38,-346",
        ["ElvUF_RaidMover"] = "BOTTOMLEFT,ElvUIParent,BOTTOMLEFT,4,298",
        ["LeftChatMover"] = "BOTTOMLEFT,ElvUIParent,BOTTOMLEFT,4,4",
        ["GMMover"] = "TOPLEFT,ElvUIParent,TOPLEFT,250,-5",
        ["BuffsMover"] = "TOPRIGHT,MMHolder,TOPLEFT,-7,-1",
        ["MirrorTimer3Mover"] = "TOP,MirrorTimer2,BOTTOM,0,0",
        ["BossButton"] = "BOTTOM,ElvUIParent,BOTTOM,-1,293",
        ["LootFrameMover"] = "TOPLEFT,ElvUIParent,TOPLEFT,250,-104",
        ["ZoneAbility"] = "BOTTOM,ElvUIParent,BOTTOM,-1,293",
        ["SocialMenuMover"] = "TOPLEFT,ElvUIParent,TOPLEFT,4,-187",
        ["ElvUF_AssistMover"] = "TOPLEFT,ElvUIParent,TOPLEFT,4,-248",
        ["ElvUF_PetMover"] = "BOTTOM,ElvUIParent,BOTTOM,-342,100",
        ["ElvUF_TargetMover"] = "BOTTOM,ElvUIParent,BOTTOM,342,139",
        ["ElvUF_FocusMover"] = "BOTTOM,ElvUIParent,BOTTOM,342,59",
        ["RightChatMover"] = "BOTTOMRIGHT,ElvUIParent,BOTTOMRIGHT,-4,4",
        ["ElvUF_PlayerMover"] = "BOTTOM,ElvUIParent,BOTTOM,-342,139",
        ["ElvUIBagMover"] = "BOTTOMRIGHT,RightChatPanel,BOTTOMRIGHT,0,26",
        ["DurabilityFrameMover"] = "TOPRIGHT,ElvUIParent,TOPRIGHT,-135,-300",
        ["ElvUF_PetCastbarMover"] = "TOPLEFT,ElvUF_Pet,BOTTOMLEFT,0,-1",
        ["VehicleSeatMover"] = "TOPLEFT,ElvUIParent,TOPLEFT,4,-4",
        ["ElvUF_PlayerCastbarMover"] = "BOTTOM,ElvUIParent,BOTTOM,0,243",
        ["ExperienceBarMover"] = "BOTTOM,ElvUIParent,BOTTOM,0,43",
        ["ElvUF_Raid40Mover"] = "BOTTOMLEFT,ElvUIParent,BOTTOMLEFT,4,297",
        ["ElvUF_TargetTargetMover"] = "BOTTOM,ElvUIParent,BOTTOM,342,100",
        ["ElvUF_FocusCastbarMover"] = "TOPLEFT,ElvUF_Focus,BOTTOMLEFT,0,-1",
        ["LossControlMover"] = "BOTTOM,ElvUIParent,BOTTOM,-1,507",
        ["LevelUpBossBannerMover"] = "TOP,ElvUIParent,TOP,-1,-120",
        ["MirrorTimer1Mover"] = "TOP,ElvUIParent,TOP,-1,-96",
        ["ElvAB_6"] = "TOPRIGHT,ElvUIParent,TOPRIGHT,-4,-346",
        ["ElvAB_1"] = "BOTTOM,ElvUIParent,BOTTOM,0,139",
        ["ElvAB_2"] = "BOTTOM,ElvUIParent,BOTTOM,0,4",
        ["BelowMinimapContainerMover"] = "TOPRIGHT,ElvUIParent,TOPRIGHT,-4,-274",
        ["ElvAB_4"] = "BOTTOM,ElvUIParent,BOTTOM,-153,285",
        ["TalkingHeadFrameMover"] = "BOTTOM,ElvUIParent,BOTTOM,-1,373",
        ["AltPowerBarMover"] = "TOP,ElvUIParent,TOP,-1,-36",
        ["AzeriteBarMover"] = "TOPRIGHT,ElvUIParent,TOPRIGHT,-3,-245",
        ["ElvAB_3"] = "BOTTOM,ElvUIParent,BOTTOM,0,192",
        ["ElvAB_5"] = "BOTTOM,ElvUIParent,BOTTOM,152,285",
        ["VehicleLeaveButton"] = "BOTTOM,ElvUIParent,BOTTOM,0,300",
        ["VOICECHAT"] = "TOPLEFT,ElvUIParent,TOPLEFT,250,-82",
        ["MirrorTimer2Mover"] = "TOP,MirrorTimer1,BOTTOM,0,0",
        ["ObjectiveFrameMover"] = "TOPRIGHT,ElvUIParent,TOPRIGHT,-163,-325",
        ["BNETMover"] = "TOPRIGHT,ElvUIParent,TOPRIGHT,-4,-274",
        ["ShiftAB"] = "BOTTOMLEFT,ElvUIParent,BOTTOMLEFT,4,264",
        ["ElvUIBankMover"] = "BOTTOMLEFT,LeftChatPanel,BOTTOMLEFT,0,26",
        ["HonorBarMover"] = "TOPRIGHT,ElvUIParent,TOPRIGHT,-3,-255",
        ["ArenaHeaderMover"] = "BOTTOMRIGHT,ElvUIParent,RIGHT,-105,-165",
        ["TooltipMover"] = "BOTTOMRIGHT,RightChatToggleButton,BOTTOMRIGHT,0,0",
        ["ElvUF_TankMover"] = "TOPLEFT,ElvUIParent,TOPLEFT,4,-186",
        ["BossHeaderMover"] = "BOTTOMRIGHT,ElvUIParent,RIGHT,-105,-165",
        ["TotemBarMover"] = "BOTTOMLEFT,ElvUIParent,BOTTOMLEFT,490,4",
        ["ElvUF_TargetCastbarMover"] = "BOTTOM,ElvUIParent,BOTTOM,0,97",
        ["ElvUF_PartyMover"] = "BOTTOMLEFT,ElvUIParent,BOTTOMLEFT,4,298",
        ["AlertFrameMover"] = "TOP,ElvUIParent,TOP,-1,-18",
        ["DebuffsMover"] = "BOTTOMRIGHT,MMHolder,BOTTOMLEFT,-7,1",
        ["MinimapMover"] = "TOPRIGHT,ElvUIParent,TOPRIGHT,-3,-3",
    },
    ["auras"] = {
        ["debuffs"] = {
            ["size"] = 40,
        },
        ["buffs"] = {
            ["size"] = 40,
        },
    },
}::profile::Default